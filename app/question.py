from flask import session
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, RadioField, SelectMultipleField, widgets, SelectField
from wtforms.validators import DataRequired
from app import db
from app.models import Question as QuestionDB, Answer as AnswerDB, User as UserDB
import json

class MultiCheckboxField(SelectMultipleField):
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()

class DefaultForm(FlaskForm):
    input = StringField('', validators=[DataRequired()])
    submit = SubmitField('Отправить')

class RadioForm(FlaskForm):
    input = RadioField('', choices=[])
    submit = SubmitField('Отправить')
    def __init__(self, args):
        super().__init__()
        self.input.choices = tuple(json.loads(args).items())

class SelectForm(FlaskForm):
    input = SelectField('', choices=[])
    submit = SubmitField('Отправить')

    def __init__(self, args):
        super().__init__()
        self.input.choices = tuple(json.loads(args).items())

class CheckboxForm(FlaskForm):
    input = MultiCheckboxField('', choices=[])
    submit = SubmitField('Отправить')

    def __init__(self, args):
        super().__init__()
        self.input.choices = tuple(json.loads(args).items())

class Question:

    def __init__(self):
        question_count = QuestionDB.query.count()
        if question_count == 0:
            for item in json.load(open('app/question.json')):
                q = QuestionDB(id=item['id'], quest=item['quest'], type=item['type'], answers=item['answers'])
                db.session.add(q)
            db.session.commit()
            question_count = QuestionDB.query.count()
        
        self.setQuestionsLength(question_count)

    
    def setForm(self, value):
        self.__form=value

    def setAnswer(self, id, answer,answers):
        ans=''
        if isinstance(answer, list):
            for a in answer:
                for b in json.loads(answers).items():
                    if a == b[0]:
                        ans += b[1] + '; '
        else:
            if answers == '':
                ans=answer
            else:    
                for a in json.loads(answers).items():
                    if a[0]==answer:
                        ans=a[1]
                        continue
        u = UserDB.query.filter(UserDB.id == session['user_id']).first()
        a = AnswerDB(user_id=session['user_id'], attempted_survey=u.attempted_survey, quest_id=id, answers=ans)
        db.session.add(a)
        u.last_question_id=id
        db.session.add(u)
        db.session.commit()
    
    def setResultButton(self, value):
        self.__resultBtn=value

    def setQuestionsLength(self, value):
        self.__questions_length=value

    def getForm(self): return self.__form
    def getQuestionsLength(self): return self.__questions_length
    def getNextQuestion(self, value): 
        if value + 1 == self.getQuestionsLength():
            return '/finish'
        else: 
            return '/question/' + str(value+1)

    def getQuestion(self, value):
        q = QuestionDB.query.filter(QuestionDB.id == value).first()
        return q
    
    def setFinish(self):
        u = UserDB.query.filter(UserDB.id == session['user_id']).first()
        u.attempted_survey+=1
        u.last_question_id=None
        db.session.add(u)
        db.session.commit()

    
    def getQuestionsResult(self):
        a = AnswerDB.query.join(QuestionDB, (QuestionDB.id == AnswerDB.quest_id)).add_columns(AnswerDB.attempted_survey, AnswerDB.answers,QuestionDB.quest).filter(AnswerDB.user_id == session['user_id']).all()
        result = []
        temp = []
        attempted_survey = 0
        for ans in a:
            if ans.attempted_survey == attempted_survey:
                temp.append({'quest': ans.quest, 'answer': ans.answers})
            else:
                result.append(temp)
                temp=[]
                temp.append({'quest': ans.quest, 'answer': ans.answers})
                attempted_survey = ans.attempted_survey
        result.append(temp) 
        return result

   
        
    
    
