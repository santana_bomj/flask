from app import db

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))
    patronymic = db.Column(db.String(64))
    attempted_survey = db.Column(db.Integer, default=0)
    last_question_id = db.Column(db.Integer)

    def __repr__(self):
        return '<User {} {} {} {} {} {}>'.format(self.id, self.last_name, self.first_name, self.patronymic, self.attempted_survey, self.last_question_id) 

class Question(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    quest = db.Column(db.String(512))
    type = db.Column(db.String(64))
    answers = db.Column(db.String(512))

    def __repr__(self):
        return '<Question {} {} {} {}>'.format(self.id, self.quest, self.type, self.answers) 

class Answer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    attempted_survey = db.Column(db.Integer)
    quest_id = db.Column(db.Integer, db.ForeignKey('question.id'))
    answers = db.Column(db.String(512))

    def __repr__(self):
        return '<Answer {} {} {} {} {} {}>'.format(self.id, self.user_id, self.attempted_survey, self.quest_id, self.answers) 