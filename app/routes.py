from flask import render_template, redirect, url_for, session
import json
from app import app
from app.question import Question, DefaultForm, RadioForm, SelectForm, CheckboxForm
from app.user import UserForm, User

questions = Question()

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    userForm = UserForm()
    user = User(session)
    userInfo = user.getUser()
    continueBtn = user.getLastQuestionId()
    resultBtn = user.getResultBtn()
    if userForm.validate_on_submit() and userForm.data['submit']:
        session['first_name'] = userForm.data['first_name']
        session['last_name'] = userForm.data['last_name']
        session['patronymic'] = userForm.data['patronymic']
        return redirect(url_for('index'))
    return render_template('index.html', user=userInfo, form=userForm, resultBtn=resultBtn, continueBtn=continueBtn)

@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))

@app.route('/question/<int:question_id>', methods=['GET', 'POST'])
def question(question_id):
    if 'user_id' not in session:
        return redirect(url_for('index'))
    question_temp = questions.getQuestion(question_id)
    if question_temp == None:
        return redirect(url_for('index'))
    if question_temp.type == 'default':
        questions.setForm(DefaultForm())
    elif question_temp.type == 'radio':
        questions.setForm(RadioForm(question_temp.answers))
    elif question_temp.type == 'select':
        questions.setForm(SelectForm(question_temp.answers))
    elif question_temp.type == 'checkbox':
        questions.setForm(CheckboxForm(question_temp.answers))

    question = questions.getForm()
    if question.validate_on_submit() and question.data['submit']:
        questions.setAnswer(question_id, question.data['input'], question_temp.answers)
        return redirect(questions.getNextQuestion(question_id))
    return render_template('question.html', quest=question_temp.quest,question=question)

@app.route('/finish', methods=['GET', 'POST'])
def finish():
    if 'user_id' not in session:
        return redirect(url_for('index'))
    questions.setFinish()
    return render_template('finish.html')

@app.route('/result', methods=['GET', 'POST'])
def result():
    if 'user_id' not in session:
        return redirect(url_for('index'))
    res = questions.getQuestionsResult()
    return render_template('result.html', res=res)
