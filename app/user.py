from flask import session
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired
from app import db
from app.models import User as UserDB

class UserForm(FlaskForm):
    first_name = StringField('Имя', validators=[DataRequired()])
    last_name = StringField('Фамилия', validators=[DataRequired()])
    patronymic = StringField('Отчество', validators=[DataRequired()])
    submit = SubmitField('Начать')

class User:
    def __init__(self, session):
        super().__init__()
        if 'first_name' in session and 'last_name' in session and 'last_name' in session:
            self.setFirstName(session['first_name'])
            self.setLastName(session['last_name'])
            self.setPatronymic(session['patronymic'])
            self.setAuthorized(True)
            u = UserDB.query.filter(UserDB.first_name == session['first_name'] and UserDB.last_name == session['last_name'] and UserDB.patronymic == session['patronymic']).first()
            if u != None:
                session['user_id']=u.id
                self.setLastQuestionId(u.last_question_id)
                if u.attempted_survey != 0:
                    self.setResultButton(True)
                else:
                    self.setResultButton(False)
            else:
                self.createUser(session['first_name'], session['last_name'], session['patronymic'])
                self.setResultButton(False)
                self.setLastQuestionId(None)
        else:
            self.setFirstName(None)
            self.setLastName(None)
            self.setPatronymic(None)
            self.setAuthorized(False)
            self.setUserId(None)
            self.setLastQuestionId(None)
            self.setResultButton(False)

    
    def setFirstName(self, value):
        self.__first_name=value
    def setLastName(self, value):
        self.__last_name=value
    def setPatronymic(self, value):
        self.__patronymic=value
    def setAuthorized(self, value):
        self.__authorized=value
    def setUserId(self, value):
        self.__user_id=value
    def setLastQuestionId(self, value):
        self.__last_question_id=value
    def setResultButton(self, value):
        self.__resultBtn=value


    def getFirstName(self): return self.__first_name
    def getLastName(self): return self.__last_name
    def getPatronymic(self): return self.__patronymic
    def getAuthorized(self): return self.__authorized
    def getUserId(self): return self.__user_id
    def getLastQuestionId(self): return self.__last_question_id
    def getResultBtn(self): return self.__resultBtn

    def getUser(self):
        user =  {
                    'first_name': self.getFirstName(), 
                    'last_name': self.getLastName(),
                    'patronymic': self.getPatronymic(),
                    'authorized': self.getAuthorized()
                }
        return user

    def createUser(self, first_name, last_name, patronymic):
        u = UserDB(first_name=first_name, last_name=last_name, patronymic=patronymic)
        db.session.add(u)
        db.session.flush()
        db.session.refresh(u)
        session['user_id']=u.id
        db.session.commit()